<?php 
/*
  Plugin Name: Partners Plugin
  Author: Gregorio
  Version: 0.1
*/


// add_action("admin_menu", "addMenu");
// function addMenu()
// {
//   add_menu_page("Partners", "Partners", 4, "example-options", "menuPartnairs", 'dashicons-chart-pie');
// }



function wpm_custom_post_type() {

	// On rentre les différentes dénominations de notre custom post type qui seront affichées dans l'administration
	$labels = array(
		// Le nom au pluriel
		'name'                => _x( 'Partenaire', 'Post Type General Name'),
		// Le nom au singulier
		'singular_name'       => _x( 'Partenaire', 'Post Type Singular Name'),
		// Le libellé affiché dans le menu
		'menu_name'           => __( 'Partenaire'),
		// Les différents libellés de l'administration
		'all_items'           => __( 'Toutes les Partenaire'),
		'view_item'           => __( 'Voir les Partenaire'),
		'add_new_item'        => __( 'Ajouter une nouvelle Partenaire'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer la Partenaire'),
		'update_item'         => __( 'Modifier la Partenaire'),
		'search_items'        => __( 'Rechercher une Partenaire'),
		'not_found'           => __( 'Non trouvée'),
		'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
	);
	
	// On peut définir ici d'autres options pour notre custom post type
	
	$args = array(
		'label'               => __( 'Partenaire'),
		'description'         => __( 'Tous sur Partenaire'),
		'labels'              => $labels,
		'menu_icon'           => 'dashicons-video-alt2',
		// On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		/* 
		* Différentes options supplémentaires
		*/	
		'show_in_rest' => true,
		'hierarchical'        => false,
		'public'              => true,
		'has_archive'         => true,
		'rewrite'			  => array( 'slug' => 'partenaires'),

	);
	
	// On enregistre notre custom post type qu'on nomme ici "serietv" et ses arguments
	register_post_type( 'partenaires', $args );

}

add_action( 'init', 'wpm_custom_post_type', 0 );
?>



